(function(window, document) {
    jQuery(document).ready(function($) {
        var scrollbarWidth = 0,
            originalMargin, touchHandler = function(event) {
                event.preventDefault();
            };

        function getScrollbarWidth() {
            if (scrollbarWidth) return scrollbarWidth;
            var scrollDiv = document.createElement('div');
            $.each({
                top: '-9999px',
                width: '50px',
                height: '50px',
                overflow: 'scroll',
                position: 'absolute'
            }, function(property, value) {
                scrollDiv.style[property] = value;
            });
            $('body').append(scrollDiv);
            scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
            $('body')[0].removeChild(scrollDiv);
            return scrollbarWidth;
        }
    });
})(window, document);
(function(window, document) {
    var detectDevice = {
        init: function() {
            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                $("body").addClass('iphone');
            } else {
                $("body").removeClass('iphone');
            }
        }
    }
    var _instance = '',
        _instanceMobile = '';
    var imageLazy = {
        init: function() {
            _instance = $('.lazy:visible').lazy({
                effect: "fadeIn",
                chainable: false,
                effectTime: 300,
                threshold: 10,
                afterLoad: function(element) {
                    element.removeClass("lazy").addClass('loaded').parent().addClass('is-overlay');
                    if($(element).parents(".box").length) {
                        $(element).parents(".box").find('.btn.btn-play').addClass('on');
                    }
                    
                },
            });
            var _fl = true;
            $(window).resize(function(event) {
                if(_fl) {
                    _instanceMobile = $('.lazy').lazy({
                        effect: "fadeIn",
                        chainable: false,
                        effectTime: 300,
                        threshold: 10,
                        afterLoad: function(element) {
                            element.removeClass("lazy").addClass('loaded').parent().addClass('is-overlay');;
                        },
                    });
                    _fl = false;
                } else {
                    _instance.addItems(".lazy:visible").update(true);
                }
            });
        }
    }
    var slider = {
        init: function () {
            slider.heroSlider();
        },
        heroSlider: function (argument) {
            var _ele = $(".js-hero-slider");
            if(_ele.length == 0) return false;
            _ele.on('init', function(event, slick) {
                $(window).resize();
            });
            _ele.slick({
                infinite: false,
                slidesToShow: 5,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                cssEase: 'cubic-bezier(0.165, 0.84, 0.44, 1)',
                prevArrow: '.js-hero-slider-prev',
                nextArrow: '.js-hero-slider-next',
                responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                $(window).resize();
            });
        }
    }
    var equalHeight = {
        init: function (argument) {
            var _options = {
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            }
            // $('[panel-link-data-heading]').matchHeight(_options);
        }
    }
    var header = {
        init: function () {
            header.hamburger();
            header.search();
            header.subNav();
        },
        hamburger: function() {
            var _ele = $("[data-hamburger]");
            _ele.on('click', function(event) {
                event.preventDefault();
                var _this = $(this);
                if (_this.attr('aria-expanded') == 'true') {
                    $("html").removeClass('nav-expand');
                    $("body").removeClass('nav-expand').find('header .nav').slideUp(400);
                    setTimeout(function() {
                        _this.attr('aria-expanded', 'false');
                    }, 450);
                } else {
                    $("html").addClass('nav-expand');
                    $("body").addClass('nav-expand').find('header .nav').slideDown(400);
                    setTimeout(function() {
                        _this.attr('aria-expanded', 'true');
                    }, 450);
                }
            });
        },
        search: function() {
            var _ele = $("header [data-search]");
            _ele.on('click', function(event) {
                var _this = $(this);
                if (_this.attr('aria-expanded') == 'true') {
                } else {
                    event.preventDefault();
                     _this.parent().addClass('on');
                    setTimeout(function() {
                        _this.attr('aria-expanded', 'true');
                    }, 450);
                }
            });
        },
        subNav: function() {
            var _nav = $("[data-nav]");
            _nav.find('> li').hoverIntent({
                over: handlerNavIn,
                out: handlerNavOut,
                timeout: 300
            });
            
            var _flagMouse = false;
            function handlerNavIn() {
                
                if($(window).outerWidth() > 1023) {
                    var _this = $(this);
                    
                    if(_this.find('[data-sub-nav]').length) {
                        _flagMouse = true;
                        _this.addClass('is-hover');
                        _this.find('[data-sub-nav]').addClass('visible').addClass('on').css('z-index', '3');
                    } else {
                        _this = _nav;
                        _this.find('>li').removeClass('is-hover');
                        _this.find('[data-sub-nav]').removeClass('on');
                        setTimeout(function() {
                            _this.find('[data-sub-nav]').removeClass('visible');
                        }, 250);
                    }
                }
            }

            function handlerNavOut() {
                var _this = $(this);
                if($(window).outerWidth() > 1023) {
                    _this.find('[data-sub-nav]').css('z-index', '2');
                }
            }

            _nav.mouseleave(function(event) {
                console.log(1);
                if($(window).outerWidth() > 1023) {
                    var _this = $(this);
                    
                    if(_this.find('[data-sub-nav]').length) {
                        _this.find('>li').removeClass('is-hover');
                        _this.find('[data-sub-nav]').removeClass('on');
                        setTimeout(function() {
                            _this.find('[data-sub-nav]').removeClass('visible');
                        }, 250);
                    }
                }
            });

            var _expand = $("[data-nav] [data-expand]");
            _expand.on('click', function(event) {
                event.preventDefault();
                var _this = $(this);
                if (_this.attr('aria-expanded') == 'true') {
                    _this.parent().removeClass('on').find('.ul-dropdown').slideUp(400);
                    setTimeout(function() {
                        _this.attr('aria-expanded', 'false');
                    }, 450);
                } else {
                    _this.parent().addClass('on').find('.ul-dropdown').slideDown(400);
                    setTimeout(function() {
                        _this.attr('aria-expanded', 'true');
                    }, 450);
                }
            });
        }
    }
    var footer = {
        init: function() {
            footer.scrollToTop();
        },
        scrollToTop: function() {
            var _ele = $(".back-to-top");
            _ele.on('click', function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }
    }

    jQuery(document).ready(function($) {
        detectDevice.init();
        imageLazy.init();
        slider.init();
        header.init();
    });
})(window, document);