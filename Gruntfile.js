module.exports = function (grunt) {
    var getUniqueId = function() {
        return new Date().getTime();
    }
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            sass: {
                files: ['css/**/*.scss'],
                tasks: ['sass', 'regex-replace:cachebustcss']
            },
            copy: {
                files: ['fonts/**/*.*', 'images/**/*.*'],
                tasks: ['copy']
            },
            js: {
                files: ['js/**/*.js'],
                tasks: ['concat', 'regex-replace:cachebustjs'],
                options: {
                    spawn: false
                }
            },
            output_twig: {
                files: ['html/_static/**/*.html'],
                tasks: ['output_twig']
            }
        },
        sass: {
            dist: {                            
                options: {                       
                    style: 'compressed',
                    sourcemap: 'none'
                },
                files: {                  
                    'public/css/all.css': ['css/all.scss']
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 8', 'ie 9']
            },
            multiple_files: {
                expand: true,
                flatten: true,
                src: 'public/css/*.css',
                dest: 'public/css/'
            },
        },
        concat: {
            app: {
                src: [
                    'js/vendor/jquery-3.2.1.min.js',
                    'js/vendor/smoothscroll.js',
                    'js/vendor/slick.js',
                    'js/vendor/jquery.lazy.min.js',
                    'js/vendor/validator.js',
                    'js/vendor/jquery.matchHeight.js',
                    'js/vendor/jquery.hoverIntent.min.js',
                    'js/app.js'
                ],
                dest: 'public/js/all.js'
            }
        },
        browserSync: {
            bsFiles: {
                src : [
                    'public/css/*.css',
                    'public/js/*.js',
                    'public/images/*.*',
                    'public/fonts/*.*',
                    'public/static/*.*',
                ]
            },
            options: {
                watchTask: true,
                server: {
                    baseDir: "public"
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: ['**/*.*'],
                    dest: 'public/images/'
                }]
            }
        },
        uglify: {
            app: {
                src: [
                    'js/vendor/jquery-3.2.1.min.js',
                    'js/vendor/smoothscroll.js',
                    'js/vendor/slick.js',
                    'js/vendor/jquery.lazy.min.js',
                    'js/vendor/validator.js',
                    'js/vendor/jquery.matchHeight.js',
                    'js/vendor/jquery.hoverIntent.min.js',
                    'js/app.js'
                ],
                dest: 'public/js/all.js'
            }
        },
        copy: {
            pie: {
                src: 'css/PIE.htc',
                dest: 'public/css/PIE.htc'
            },
            font: {
                expand: true,
                src: ['fonts/**/*'],
                dest: 'public/',
                filter: 'isFile'
            },
            images: {
                expand: true,
                cwd: 'images',
                src: ['**/*.*'],
                dest: 'public/images/'
            }
        },
        output_twig: {
            options: {
                docroot: 'html/_static/'
            },
            files: {
                expand: true,
                cwd: 'html/_static/',
                src: ['*.html'],
                dest: 'public/static/',
                ext: '.html'
            },
        },
        'regex-replace': {
            cachebustcss: {
                src: [
                    'html/_static/_includes/foot.html',
                    'html/_static/_includes/head.html'
                ],
                actions: [
                    {
                        search: /(.css\?v=)\d+?(")/g,
                        replace: '$1' + getUniqueId() + '$2'
                    }
                ]
            },
            cachebustjs: {
                src: [
                    'html/_static/_includes/foot.html',
                    'html/_static/_includes/head.html'
                ],
                actions: [
                    {
                        search: /(.js\?v=)\d+?(")/g,
                        replace: '$1' + getUniqueId() + '$2'
                    }
                ]
            },
        }

    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-output-twig');
    grunt.loadNpmTasks('grunt-regex-replace');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.registerTask('build', ['sass', 'autoprefixer', 'concat', 'uglify', 'copy', 'imagemin', 'regex-replace']);
    grunt.registerTask('dev', ['sass', 'autoprefixer', 'concat', 'uglify', 'copy', 'imagemin', 'regex-replace', 'output_twig', 'browserSync', 'watch']);
};